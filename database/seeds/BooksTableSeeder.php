<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new \App\Book();
        $data->isbn = "42342asaed432423";
        $data->title = "Programowanie na Javie";
        $data->author = "Król Julian";
        $data->type = "Horror, Komedia";
        $data->pages = "3100";
        $data->release_date = 1524149794;
        $data->save();

        $data = new \App\Book();
        $data->isbn = "23423423";
        $data->title = "Programowanie na PHP";
        $data->author = "Adam Mickiewicz";
        $data->type = "Dramat sportowy";
        $data->pages = "342";
        $data->release_date = 1524149794;
        $data->save();
    }
}
