<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new \App\Type();
        $data->name="Horror";
        $data->save();

        $data = new \App\Type();
        $data->name="Dramat";
        $data->save();

        $data = new \App\Type();
        $data->name="Poradnik";
        $data->save();
    }
}
