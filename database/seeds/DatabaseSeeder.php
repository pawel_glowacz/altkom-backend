<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call('BooksTableSeeder');
         $this->call('TypesTableSeeder');
         $this->call('BookTypesTableSeeder');
    }
}
