<?php

use Illuminate\Database\Seeder;

class BookTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new \App\BookType();
        $data->type_id = 1;
        $data->book_id = 1;
        $data->save();

        $data = new \App\BookType();
        $data->type_id = 2;
        $data->book_id = 1;
        $data->save();

        $data = new \App\BookType();
        $data->type_id = 2;
        $data->book_id = 2;
        $data->save();

        $data = new \App\BookType();
        $data->type_id = 3;
        $data->book_id = 2;
        $data->save();
    }
}
