<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BookRepository;
use App\Services\BookService;

class BookController extends Controller
{
    protected $bookRepository;
    protected $bookService;

    public function __construct(BookRepository $bookRepository, BookService $bookService)
    {
        $this->bookRepository = $bookRepository;
        $this->bookService = $bookService;
    }

    public function index()
    {
        // Test comment
        $books = Book::with('types')->get();
        return response()->json($books);
    }

    public function get($id)
    {
        $book = Book::with('types')->where('id', $id)->get();
        return response()->json($book);
    }

    public function search(Request $request)
    {
        $book = $this->bookRepository->searchBook($request->search);
        return response()->json($book);
    }

    public function save(Request $request)
    {
        $data = $request->all();
        $book = $this->bookService->addBook($data);
        return response()->json($book);
    }

    public function delete($id)
    {
        $book = Book::find($id);
        $book->delete();
        $books = Book::with('types')->get();
        return response()->json($book);
    }

    public function update(Request $request, $id)
    {
        $book = Book::find($id);
        $data = $request->all();
        $book->fill($data);
        $book->save();
        return response()->json('success');
    }

}
