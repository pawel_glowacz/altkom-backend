<?php

namespace App\Services;

use App\Book;

class BookService
{
    public function addBook($data)
    {
        $book = new Book();
        $book->fill($data);
        $book->save();
        return $book;
    }
}


