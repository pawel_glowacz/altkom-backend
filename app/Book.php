<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Book extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'isbn','author','type','pages','release_date'];

    protected $dates = ['release_date'];

    public function types(){
        return $this->belongsToMany(Type::class);
    }
}