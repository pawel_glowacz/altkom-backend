<?php
namespace App\Repositories;
use App\Simc;
use App\Terc;
use App\User;
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 14.10.2017
 * Time: 12:40
 */
class BookRepository{

    public function searchBook($search){
        $books = Book::query();

        if (!empty($search['title'])) {
            $books->select('books.*')
                ->where('books.title', 'LIKE', '%' . $search['name'] . '%');
        }

        return $books->orderBy('books.id')->get();

    }
}